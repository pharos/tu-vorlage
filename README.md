TU-Vorlage
========

LaTeX-Vorlage für [TU (Texte und Untersuchungen zur Geschichte der
altchristlichen Literatur)][] – [Vorlagenbeschreibung][]

[Vorlagenbeschreibung]: http://www.degruyter.com/staticfiles/pdfs/DG_GW_Richtlinien_17x24_dt.pdf

[TU (Texte und Untersuchungen zur Geschichte der
altchristlichen Literatur)]: http://www.degruyter.com/view/serial/16693

